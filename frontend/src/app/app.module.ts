import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { DashboardComponent } from './dashboard/dashboard.component'
import { LoginComponent } from './login/login.component'
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
  import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPaginationModule } from 'ngx-pagination';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HistoriqueComponent } from './historique/historique.component';

import { SidebarComponent } from './sidebar/sidebar.component';
import { MainComponent } from './shared/main/main.component';
import { ModalmodifmotdepasseComponent } from './shared/modalmodifmotdepasse/modalmodifmotdepasse.component';
import { ParamArrosageComponent } from './shared/param-arrosage/param-arrosage.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';






@NgModule({
  declarations: [
    AppComponent,
    HistoriqueComponent,
    DashboardComponent,
    LoginComponent,
    SidebarComponent,
    MainComponent,
    ModalmodifmotdepasseComponent,
    ParamArrosageComponent

   ],

  imports: [BrowserModule,ReactiveFormsModule,FormsModule, AppRoutingModule, NgxPaginationModule, Ng2SearchPipeModule,HttpClientModule],
  providers: [],
  bootstrap: [AppComponent],


})
export class AppModule {}
