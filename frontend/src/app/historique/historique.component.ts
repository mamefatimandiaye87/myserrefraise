 import prof from '../histo.json';

import { Component,OnInit} from '@angular/core';
export interface Histo{
  Date: string
  Temperature: string
  HumiditeA: string
  HumiditeS: string
  Luminosite: string
}

@Component({
  selector: 'app-historique',
  templateUrl: './historique.component.html',
  styleUrls: ['./historique.component.css'],
  
})
export class HistoriqueComponent implements OnInit {
[x: string]: any;
  filterTerm!: string;
  profilhis: any=[];
  totalLenght: any;
  page : number=1;
  itemsPerPage:number=6;

  ngOnInit(): void {
   this.historiques=prof;
   console.log(this.historiques)
  }

  search(e:any) {
    console.log(e.target.value)
    this.historiques=this.historiques.filter((el:any)=>{
      return el.Date.toLowerCase().includes(e.target.value.toLowerCase())

    })
      /* recherche */
  }

  historiques: Histo[]= prof
}


