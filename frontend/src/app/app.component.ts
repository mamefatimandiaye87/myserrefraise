import { Component } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'mon-application-angular';
  filterTerm!: string;
  show:any;
  historiques = [{

    "Date": "3/03/2023",

      "Temperature": "23°C",

      "HumiditeA": "46%",

     "HumiditeS": "45%",

        "Luminosite": "45Lux"
  },
  {
    "Date": "4/03/2023",

  "Temperature": "23°C",

  "HumiditeA": "46%",

 "HumiditeS": "45%",

    "Luminosite": "45Lux"

},
{
  "Date": "5/03/2023",

"Temperature": "23°C",

"HumiditeA": "46%",

"HumiditeS": "45%",

  "Luminosite": "45Lux"

},
{
  "Date": "6/03/2023",

"Temperature": "23°C",

"HumiditeA": "46%",

"HumiditeS": "45%",

  "Luminosite": "45Lux"

},
{
"Date": "7/03/2023",

"Temperature": "23°C",

"HumiditeA": "46%",

"HumiditeS": "45%",

"Luminosite": "45Lux"

}
,
{
  "Date": "8/03/2023",

"Temperature": "23°C",

"HumiditeA": "46%",

"HumiditeS": "45%",

  "Luminosite": "45Lux"

},
{
"Date": "8/03/2023",

"Temperature": "23°C",

"HumiditeA": "46%",

"HumiditeS": "45%",

"Luminosite": "45Lux"

}
,
{
"Date": "10/03/2023",

"Temperature": "23°C",

"HumiditeA": "46%",

"HumiditeS": "45%",

"Luminosite": "45Lux"

}
 ]

 constructor(private router: Router){

  router.events.forEach((event) => {
    if (event instanceof NavigationStart) {
      if (event.url === '/log-in'|| event.url === '/') {
        this.show = false;
      } else {
        this.show = true;

      }
    }
  });
}
}
