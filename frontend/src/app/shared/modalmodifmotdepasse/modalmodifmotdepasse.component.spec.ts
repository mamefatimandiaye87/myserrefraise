import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalmodifmotdepasseComponent } from './modalmodifmotdepasse.component';

describe('ModalmodifmotdepasseComponent', () => {
  let component: ModalmodifmotdepasseComponent;
  let fixture: ComponentFixture<ModalmodifmotdepasseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalmodifmotdepasseComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ModalmodifmotdepasseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
